const http=require("http");
const { request } = require("https");
let url = require("url")
const port = 3000
const server= http.createServer((req, res) =>{
    if (req.url == '/login'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("You are in the Login Page");
    }else
        res.writeHead(404,{'Content-Type':'text/plain'});
        res.end("page not available");
})
server.listen(port) 
console.log(`Server running at localhost: ${port}`);
